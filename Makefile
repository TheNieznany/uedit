fflags = -fmax-errors=4 -fno-common
Wflags = -Wall -Wdouble-promotion -Wextra -Wformat=2 -Wformat-overflow -Wformat-truncation -Wshadow -Wstack-usage=8192 -Wundef

debug:
	g++ main.cpp -std=c++11 -g $(fflags) $(Wflags) -lncurses -pedantic -o uedit
