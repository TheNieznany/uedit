/*
 * Simple text editor in terminal for Linux.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit.
 *
 * uedit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit.  If not, see <https://www.gnu.org/licenses/>.
 */

bool readFile(const string& _filename)
{
	int c;

	FILE* f = fopen(_filename.c_str(), "r");
	if (f == NULL) return false;

	do
	{
		c = fgetc(f);

		switch (c)
		{
		case '\n':
			lines.push_back("");
			cCursor.line++;
			cCursor.index = 0;
		break;

		default:
			if ((32 <= c && c <= 126) || c == TAB)
			{
				lines[cCursor.line].push_back((char)c);
				cCursor.index++;
			}
		break;
		}
	}
	while (c != EOF);

	filename = _filename;
	newFile = false;

	fclose(f);
	return true;
}

bool writeFile(const string& _filename)
{
	FILE* f = fopen(_filename.c_str(), "w");
	if (f == NULL) return false;

	for (size_t i = 0; i < lines.size(); i++)
	{
		for (size_t j = 0; j < lines[i].size(); j++)
		{
			fputc((int)lines[i][j], f);
		}
		if (i != lines.size() - 1)
		{
			fputc('\n', f);
		}
	}

	fclose(f);
	return true;
}





void savePrompt(bool quitPrompt = false)
{
	char tmp[32];
	int choice;

	clear();



	if ((!newFile && !quitPrompt) || (!newFile && !dirty && quitPrompt))
	{
		writeFile(filename);
		dirty = false;
		return;
	}
	else if (!newFile && quitPrompt)
	{
		addstr("Save this file? [Y/N]");
		while (true)
		{
			choice = getch();

			if (choice == 'Y' || choice == 'y')
			{
				writeFile(filename);
				dirty = false;
				return;
			}
			else if (choice == 'N' || choice == 'n')
			{
				return;
			}
		}
	}



	addstr("Save this file? [Y/N]");

	while (true)
	{
		choice = getch();

		if (choice == 'Y' || choice == 'y')
		{
			addstr("\nEnter file name: ");
			getstr(tmp);

			if   (tmp[0] == '\0') filename = "untitled.txt";
			else filename = tmp;

			newFile = false;

			FILE* f = fopen(tmp, "r");
			if (f != NULL)
			{
				addstr("File exists, overwrite it? [Y/N]");

				while (true)
				{
					choice = getch();

					if (choice == 'Y' || choice == 'y')
					{
						writeFile(tmp);
						if (dirty) dirty = false;
						fclose(f);
						return;
					}
					else if (choice == 'N' || choice == 'n')
					{
						fclose(f);
						return;
					}
				}
			}
			else
			{
				writeFile(tmp);
				if (dirty) dirty = false;
				return;
			}
		}
		else if (choice == 'N' || choice == 'n')
		{
			return;
		}
	}
}
