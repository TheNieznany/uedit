/*
 * Simple functions made to help with debugging and testing.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DEBUG_C
#define DEBUG_C

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <time.h>

FILE* debug_f;
time_t debug_now2;
struct tm* debug_now;

clock_t debug_start;
clock_t debug_stop;

char debug_msg[64];



/**
 * Writes to "debug.log" file in format:
 * DD-MM-YYYY HH-MM-SS   [_type] _message
 *
 * @param _type Custom log type, can be anything
 * @param _message Custom log message, can be anything
 */
void debug_log(const char* _type, const char* _message)
{
	debug_now2 = time(0);
	debug_now = localtime(&debug_now2);

	fprintf(debug_f, "%02d-%02d-%04d %02d:%02d:%02d   [%s] %s\n",
		debug_now->tm_mday, debug_now->tm_mon+1, debug_now->tm_year+1900,
		debug_now->tm_hour, debug_now->tm_min, debug_now->tm_sec,
		_type, _message);
}



/**
 * Initializes program, must call before other functions
 */
void debug_init()
{
	debug_f = fopen("debug.log", "a");
	debug_log("INIT", "debug init");
}


/**
 * Deinitializes program, should be called at exit
 */
void debug_quit()
{
	debug_log("QUIT", "debug quit");
	fputc('\n', debug_f);
	fclose(debug_f);
}



/**
 * Saves current time, used with debug_timingStop()
 */
void debug_timingStart()
{
	debug_start = clock();
}

/**
 * Calculates time since call to debug_timingStart()
 * and writes to "debug.log" file in format:
 * DD-MM-YYYY HH-MM-SS   [TIMING] _name <time elapsed> ms
 *
 * @param _name Custom name for timing log, used for convenience
 */
void debug_timingStop(const char* _name)
{
	debug_stop = clock() - debug_start;
	double timeElapsed = ((double)debug_stop / CLOCKS_PER_SEC) * 1000.0;

	sprintf(debug_msg, "%s %.3f ms", _name, timeElapsed);
	debug_log("TIMING", debug_msg);
}

#ifdef __cplusplus
}
#endif

#endif
