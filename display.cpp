/*
 * Simple text editor in terminal for Linux.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit.
 *
 * uedit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit.  If not, see <https://www.gnu.org/licenses/>.
 */

size_t index2 = 0;
size_t prevLine = 0;

void display()
{
	char c;

	clear();
	attron(COLOR_PAIR(1));

	if (dirty)	addch('*');
	else 		addch(' ');

	printw(" %s", filename.c_str());

	for (uint8_t i = 0; i < (COLS - 1) - (filename.size() + 2); i++)
	{
		addch(' ');
	}

	addch('\n');

	attroff(COLOR_PAIR(1));

	for (uint8_t i = 0; i < LINES - 2 && i < lines.size(); i++)
	{
		size_t tempIndex = 0;

		for (uint8_t j = 0; j < COLS - 1 && j < lines[i + cScroll.line].size(); j++)
		{
			if (lines[i + cScroll.line].empty()) break;

			if ((i + cScroll.line) == cCursor.line && cScroll.index != 0)
			{
				c = lines[i + cScroll.line][j + cScroll.index];
			}
			else
			{
				c = lines[i + cScroll.line][j];
			}

			if (c == TAB)
			{
				if (tempIndex == 0 || tempIndex % TAB_SIZE == 0)
				{
					for (uint8_t k = 0; k < TAB_SIZE; k++)
					{
						addch(' ');
						tempIndex++;
					}
				}
				else for (size_t l = tempIndex; l % TAB_SIZE != 0; l++)
				{
					addch(' ');
					tempIndex++;
				}
			}
			else
			{
				addch(c);
				tempIndex++;
			}
		}

		addch('\n');
	}





	/* --- LINES --- */

	if (cCursor.line > (cTerm.line + cScroll.line))
	{
		cTerm.line += cCursor.line - (cTerm.line + cScroll.line);

		if ((cTerm.line + 1) > static_cast<size_t>(LINES) - 2)
		{
			cScroll.line += cTerm.line - (LINES - 2);
			cTerm.line = LINES - 2;
		}
	}
	else if (cCursor.line < (cTerm.line + cScroll.line))
	{
		if (cTerm.line == 0)
		{
			cScroll.line += cCursor.line - (cTerm.line + cScroll.line);
		}
		else
		{
			cTerm.line += cCursor.line - (cTerm.line + cScroll.line);
		}
	}

	if (cCursor.line != prevLine)
	{
		cScroll.index = 0;
	}



	/* --- INDEXES --- */

	if (cCursor.index == 0)
	{
		cScroll.index = 0;
	}

	if (cCursor.index > (index2 + cScroll.index))
	{
		index2 += cCursor.index - (index2 + cScroll.index);

		if (index2 > static_cast<size_t>(COLS) - 1)
		{
			cScroll.index += index2 - (COLS - 1);
			index2 = COLS - 1;
		}
	}
	else if (cCursor.index < (index2 + cScroll.index))
	{
		if (index2 == 0)
		{
			cScroll.index += cCursor.index - (index2 + cScroll.index);
		}
		else
		{
			index2 += cCursor.index - (index2 + cScroll.index);
		}
	}

	size_t spaces = 0;
	size_t spaceIndex = 0;

	for (size_t i = 0; i < index2; i++, spaceIndex++)
	{
		if (lines[cCursor.line][i] == TAB)
		{
			spaces += (TAB_SIZE - 1) - (spaceIndex % TAB_SIZE);
			spaceIndex += (TAB_SIZE - 1) - (spaceIndex % TAB_SIZE);
		}
	}

	cTerm.index = index2 + spaces;

	if (cTerm.index > static_cast<size_t>(COLS) - 1)
	{
		cScroll.index += cTerm.index - (COLS - 1);
		cTerm.index = COLS - 1;
	}
	else if (cCursor.index < (cTerm.index + cScroll.index))
	{
		if (cTerm.index == 0)
		{
			cScroll.index += cCursor.index - (cTerm.index + cScroll.index);
		}
	}

	prevLine = cCursor.line;

	move(cTerm.line + 1, cTerm.index);
}
