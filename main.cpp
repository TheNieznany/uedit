/*
 * Simple text editor in terminal for Linux.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit.
 *
 * uedit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdint>
#include <cstdio>

#include <string>
using std::string;

#include <vector>
using std::vector;

#include <ncurses.h>



#define TAB_SIZE 4

#define TAB			9
#define ENTER		10
#define CTRL_Q		17
#define CTRL_S		19
#define ESCAPE		27

struct Cursor { size_t index, line; };

Cursor cCursor{0, 0};
Cursor cTerm  {0, 0};
Cursor cScroll{0, 0};

vector<string> lines = {""};
string filename = "untitled.txt";
bool dirty = false;
bool newFile = true;




#include "debug.c"

#include "file-io.cpp"

void quit()
{
	savePrompt(true);
	debug_quit();
	endwin();
	exit(0);
}

#include "input.cpp"
#include "display.cpp"



int main(int argc, char** argv)
{
	debug_init();

	if (argc == 2)
	{
		if (!readFile(argv[1]))
		{
			debug_log("ERROR", "readFile() failed {false} [readfile.cpp:1]");
			quit();
		}
	}
	else if (argc != 1)
	{
		debug_log("ERROR", "program should be called with none or 1 parameter");
		quit();
	}

	initscr();
	raw();
	noecho();
	keypad(stdscr, TRUE);

	start_color();
	init_pair(1, COLOR_BLACK, COLOR_WHITE);

	while (true)
	{
		display();
		handleInput();
	}

	quit();
}
