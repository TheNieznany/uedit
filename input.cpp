/*
 * Simple text editor in terminal for Linux.
 * Copyright (C) 2021  TheNieznany <thenieznany11 at protonmail dot com>
 *
 * This file is part of uedit.
 *
 * uedit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uedit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with uedit.  If not, see <https://www.gnu.org/licenses/>.
 */

void handleInput()
{
	int c = getch();

	switch (c)
	{
	case ENTER:
		if (cCursor.line == 65535)
		{
			debug_log("WARNING", "Line limit reached ");
		}
		else if (cCursor.index < lines[cCursor.line].size())
		{
			lines.insert(lines.begin() + cCursor.line + 1, "");

			for (size_t i = cCursor.index; i < lines[cCursor.line].size(); i++)
				lines[cCursor.line + 1].push_back(lines[cCursor.line][i]);

			lines[cCursor.line].erase(cCursor.index);
			cCursor.index = 0;
			cCursor.line++;
		}
		else
		{
			cCursor.index = 0;
			cCursor.line++;
			lines.insert(lines.begin() + cCursor.line, "");
		}

		if (!dirty) dirty = true;
	break;



	case KEY_BACKSPACE:
		if (cCursor.index != 0)
		{
			lines[cCursor.line].erase(cCursor.index - 1, 1);
			cCursor.index--;
		}
		else if (cCursor.line != 0)
		{
			cCursor.index = lines[cCursor.line - 1].size();
			lines[cCursor.line - 1] += lines[cCursor.line];
			lines.erase(lines.begin() + cCursor.line);
			cCursor.line--;
		}

		if (!dirty) dirty = true;
	break;



	case KEY_DC:
		if (cCursor.index != lines[cCursor.line].size())
		{
			lines[cCursor.line].erase(cCursor.index, 1);
		}
		else if (cCursor.line != lines.size() - 1)
		{
			lines[cCursor.line] += lines[cCursor.line + 1];
			lines.erase(lines.begin() + cCursor.line + 1);
		}

		if (!dirty) dirty = true;
	break;

	case KEY_HOME:	cCursor.index = 0;							break;
	case KEY_END:	cCursor.index = lines[cCursor.line].size();	break;



	// --- ARROWS ---

	case KEY_UP:
		if (cCursor.line != 0)
		{
			cCursor.line--;
			if (lines[cCursor.line].size() < cCursor.index)
			{
				cCursor.index = lines[cCursor.line].size();
			}
		}
	break;

	case KEY_DOWN:
		if (cCursor.line != lines.size() - 1)
		{
			cCursor.line++;
			if (lines[cCursor.line].size() < cCursor.index)
			{
				cCursor.index = lines[cCursor.line].size();
			}
		}
	break;

	case KEY_LEFT:
		if (cCursor.line != 0)
		{
			if (cCursor.index == 0)
			{
				cCursor.index = lines[cCursor.line - 1].size();
				cCursor.line--;
			}
			else
			{
				cCursor.index--;
			}
		}
		else if (cCursor.index != 0)
		{
			cCursor.index--;
		}
	break;

	case KEY_RIGHT:
		if (cCursor.line != lines.size() - 1)
		{
			if (cCursor.index == lines[cCursor.line].size())
			{
				cCursor.line++;
				cCursor.index = 0;
			}
			else
			{
				cCursor.index++;
			}
		}
		else if (cCursor.index != lines[cCursor.line].size())
		{
			cCursor.index++;
		}
	break;



	// --- CTRL ---

	case CTRL_Q:
		quit();
	break;

	case CTRL_S:
		savePrompt();
	break;

	default:
		if ((32 <= c && c <= 126) || c == TAB)
		{
			lines[cCursor.line].insert(cCursor.index, 1, (char)c);
			cCursor.index++;

			if (!dirty) dirty = true;
		}
	break;
	}
}
